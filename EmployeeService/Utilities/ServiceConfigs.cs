﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeService.Utilities
{
    public static class ServiceConfigs
    {
        public static string[] Scopes { get { return "User.Read".Split(';'); } }
        public static string ClientSecret { get { return Environment.GetEnvironmentVariable("ClientSecret"); } }
        public static string TenantId { get { return Environment.GetEnvironmentVariable("TenantId"); } }
        public static string AppId { get { return Environment.GetEnvironmentVariable("AppId"); } }

        public static string CosmosEndpoint { get { return Environment.GetEnvironmentVariable("CosmosEndpoint"); } }
        public static string CosmosPrimaryKey { get { return Environment.GetEnvironmentVariable("CosmosPrimaryKey");  } }
        public static string CosmosDatabaseId { get { return Environment.GetEnvironmentVariable("CosmosDatabaseId");  } }
        public static string CosmosContainerId { get { return Environment.GetEnvironmentVariable("CosmosContainerId");  } }

        public static int DefaultLimit { get { return int.Parse(Environment.GetEnvironmentVariable("DefaultLimit")); } }
        public static int TimeoutValue { get { return int.Parse(Environment.GetEnvironmentVariable("TimeoutValue")); } }
        public static int RetryCount { get { return int.Parse(Environment.GetEnvironmentVariable("RetryCount")); } }
        public static int RetryDelay { get { return int.Parse(Environment.GetEnvironmentVariable("RetryDelay")); } }
    }
}
